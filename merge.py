# Written by Mark

# stream and search file as input file
filenames = ['eastLondonTweetStream.txt', 'southEastLondonTweetSearch.txt']

# write to the output file
with open('totalEastLondonTweets.txt','a') as outputfile:
	for file in filenames:
		with open(file) as currentFile:
			for content in currentFile:
				outputfile.write(content)

